﻿
#include <iostream>
#include <string>

using namespace std;

int main()
{
    string text{ "Grove street - Home" };

    text.front();     //выводит первый символ строки
    text.back();      //выводит последний символ строки
    text.length();    //выводит длину строки

    cout << text << endl<<endl <<"String length: " << text.length() << endl <<"First symbol: "<< text.front() << endl << "Last symbol: " << text.back() << endl;

    return 0;
}

